from pygame import *
import pyganim
import os

MOVE_SPEED = 7
WIDTH = 22
HEIGHT = 32

COLOR = "white"

JUMP_HEIGHT = 10
FORCE_GR = 0.4

ANIMATION_DELAY = 0.1
ANIMATION_RIGHT = [('hero/r1.png'),
                   ('hero/r2.png'),
                   ('hero/r3.png'),
                   ('hero/r4.png'),
                   ('hero/r5.png')]

ANIMATION_LEFT =  [('hero/l1.png'),
                   ('hero/l2.png'),
                   ('hero/l3.png'),
                   ('hero/l4.png'),
                   ('hero/l5.png')]

ANIMATION_JUMP = [('hero/j.png'), 0.1]

ANIMATION_JUMP_LEFT = [('hero/jl.png'), 0.1]

ANIMATION_JUMP_RIGHT = [('hero/jr.png'), 0.1]

ANIMATION_STAY = [('hero/o1.png'), 0.1]

class Player(sprite.Sprite):

    def __init__(self, x, y):

        sprite.Sprite.__init__(self)
        self.xlevel = 0
        self.ylevel = 0
        self.OnGround = False

        self.startX = x
        self.startY = y

        self.image = Surface((WIDTH,HEIGHT))
        self.image.fill(Color(COLOR))
        self.rect = Rect(x, y, WIDTH, HEIGHT)

        self.image.set_colorkey(Color(COLOR))
        
        boltAnim = []
        for anim in ANIMATION_RIGHT:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimRight = pyganim.PygAnimation(boltAnim)
        self.boltAnimRight.play()


        boltAnim = []
        for anim in ANIMATION_LEFT:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimLeft = pyganim.PygAnimation(boltAnim)
        self.boltAnimLeft.play()


        self.boltAnimStay = pyganim.PygAnimation(ANIMATION_STAY)
        self.boltAnimStay.play()
        self.boltAnimStay.blit(self.image, (0, 0))


        self.boltAnimJump = pyganim.PygAnimation(ANIMATION_JUMP)
        self.boltAnimJump.play()

        self.boltAnimJumpRight = pyganim.PygAnimation(ANIMATION_JUMP_RIGHT)
        self.boltAnimJumpRight.play()

        self.boltAnimJumpLeft = pyganim.PygAnimation(ANIMATION_JUMP_LEFT)
        self.boltAnimJumpLeft.play()



    def update(self, left, right, up, platforms):

        if up:
            if self.OnGround:
                self.ylevel = -JUMP_HEIGHT
                self.image.fill(Color(COLOR))
                self.boltAnimJump.blit(self.image, (0,0))


        if left:
            self.xlevel = -MOVE_SPEED
            self.image.fill(Color(COLOR))

            if up:
                self.boltAnimJumpLeft.blit(self.image, (0,0))
            else:
                self.boltAnimLeft.blit(self.image, (0,0))





        if right:
            self.xlevel = MOVE_SPEED
            self.image.fill(Color(COLOR))

            if up:
                self.boltAnimJumpRight.blit(self.image, (0,0))
            else:
                self.boltAnimRight.blit(self.image, (0,0))



        if not(left or right):
            self.xlevel =  0
            if not up:
                self.image.fill(Color(COLOR))
                self.boltAnimStay.blit(self.image, (0,0)) 


        
        if not self.OnGround:
            self.ylevel +=  FORCE_GR
        
        self.OnGround = False;   
        self.rect.y += self.ylevel
        self.collide(0, self.ylevel, platforms)

        self.rect.x += self.xlevel 
        self.collide(self.xlevel, 0, platforms)







       
    def collide(self, xlevel, ylevel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):

                if xlevel > 0:
                    self.rect.right = p.rect.left

                if xlevel < 0:
                    self.rect.left = p.rect.right

                if ylevel > 0:
                    self.rect.bottom = p.rect.top
                    self.OnGround = True
                    self.ylevel = 0

                if ylevel < 0:
                    self.rect.top = p.rect.bottom
                    self.ylevel = 0





        





















